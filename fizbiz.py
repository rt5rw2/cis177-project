"""
Fikri Ghazi
CIS177
June 24, 2016

Program: Fizbiz
Description:    A Tkinter game about finding a match for an unknown card.
                And here's the game diagram:

            Main Window _______________________________
            |                   [X]                   |  = Frame-1
            |             GUESS THE CARD!             |  = Frame-1
            |                                         |
            | [3] [4] [A] [5] [7] [K] [3] [Q] [8] [J] |  = Frame-2
            |        _________               ____     |
            |        Player_1:               Bot:     |  = Frame-3
            |           15                   20       |  = Frame-3
            |                RESET GAME               |  = Frame-footer
            |_________________________________________|
"""

from tkinter import *
from tkinter import messagebox
import random
from player import Player


class FizbizGUI:
    N_CARDS = 10  # number of side-up cards
    MAX_SCORE = 7

    def __init__(self):

        """ #######################################################################
        Game setup:
            - Create game's main window
            - Create game's main UI containers: Frame-1, Frame-2, Frame-3, Frame-footer
        """

        # Create the main Window for this GUI called mainWindow
        self.__mainWindow = Tk()
        self.__mainWindow.configure(background="white", borderwidth=50)
        self.__mainWindow.title("Fizbiz")

        # Create Frames
        frame_1 = Frame(self.__mainWindow)
        frame_2 = Frame(self.__mainWindow)
        frame_3 = Canvas(self.__mainWindow, bg='red')

        frame_footer = Frame(self.__mainWindow)

        # Attach frames to the game's main-window
        frame_1.pack()
        frame_2.pack()
        frame_3.pack(fill=BOTH, expand=1)
        frame_footer.pack()

        """ #######################################################################
        Game setup Pt.2:
            Setup game's attributes
        """

        ## Store PhotoImages to make Labels of cards ---------------------------

        self.__deckOfCards_imgs = []  # Create an empty array then,

        # Append 52 PhotoImages of cards into array
        for i in range(52):
            self.__deckOfCards_imgs.append( PhotoImage(file="cards_gif/"+str(i+1)+".gif") )
            random.shuffle(self.__deckOfCards_imgs)

        # Create a PhotoImage of a closed-card
        self.__closedCard_img = PhotoImage(file="cards_gif/b1fv.gif")

        ## Store Labels -------------------------------------------------------

        self.__deckOfCards_lbls = []  # Create an empty array to store Labels then,

        # Append 52 Labels of cards into the array
        for i in range(52):
            self.__deckOfCards_lbls.append( Label(frame_2, image=self.__deckOfCards_imgs[i]) )

        # Create a Label of a closed card
        self.__unknownCard_lbl = Label(frame_1, image=self.__closedCard_img)

        # Create answer to the unknown card
        self.__unknownCardAnswer_lbl = self.__deckOfCards_lbls[random.randint(0,self.N_CARDS-1)]

        # Track whether the unknown card is open or closed
        self.__isUnknownCardOpen = False

        # Track whether the unknown card is found or not
        self.isUnknownCardFound = False

        """ #######################################################################
        frame_1: contains a closed-card Label and a text Label
        """

        # Create text Label which will appear at the initial state of the game
        self.__gameGreeting = Label(frame_1, text="-\nGUESS THE CARD!\n", font=("Helvetica", 32))

        # Populate frame_1 with widgets
        self.__unknownCard_lbl.pack()
        self.__gameGreeting.pack()

        # When the unknown card is clicked, then open the card
        self.__unknownCard_lbl.bind("<Button-1>", self.__openTheUnknownCard)

        """ #######################################################################
        frame_2: contains n open cards, where n is the constant value of N_CARDS
        """

        # Populate n Labels of cards into Frame 2
        for i in range(self.N_CARDS):

            self.__deckOfCards_lbls[i].grid(row=0, column=i)

            # When an open card is clicked, then check if the card is a match
            self.__deckOfCards_lbls[i].bind("<Button-1>", self.__checkGuessedCard)

        """ #######################################################################
        frame_3: contains the scoreboard of two players
        """

        # Create frame_3 children frames
        frame_3_1_left = Frame(frame_3, width=1, height=1, bd=40)
        frame_3_1_right = Frame(frame_3, width=1, height=1, bd=40)

        # Create 2 players
        self.__Yoda = Player(name="YODA", score=self.MAX_SCORE, turn=True)
        self.__Robot = Player(name="R2D2", score=self.MAX_SCORE)

        # Create Yoda's box score ---------------------------------
        self.Yoda_lbl = Label(frame_3_1_left,
                         text = self.__Yoda.getPlayersName(),
                         font = ("Helvetica", 24),
                         )
        self.YodasScore_lbl = Label(frame_3_1_left,
                               text=self.__Yoda.getPlayersScores(),
                               font=("Helvetica", 24)
                               )

        # Create R2D2's box score ---------------------------------
        self.Robot_lbl = Label(frame_3_1_right,
                               text = self.__Robot.getPlayersName(),
                               font = ("Helvetica", 24),
                               #bg="blue"
                               )
        self.RobotsScore_lbl = Label(frame_3_1_right,
                           text=self.__Robot.getPlayersScores(),
                           font=("Helvetica", 24)
                           )

        # Attach box-scores to frame_3 children frames
        self.Yoda_lbl.pack()
        self.YodasScore_lbl.pack()
        self.Robot_lbl.pack()
        self.RobotsScore_lbl.pack()

        # Attach frame_3's childern frames to frame_3
        frame_3_1_left.pack(fill=BOTH, side=LEFT, expand=True)
        frame_3_1_right.pack(fill=BOTH, side=LEFT, expand=True)

        # Set Yoda to always be the first player.
        if self.__Yoda.isTurn:
            self.__changeBackgroundColor(self.Yoda_lbl, "yellow")

        """ #######################################################################
        frame_footer: only contains the reset button
        """

        # Create and attach the button to frame_footer
        reset_btn = Button(frame_footer, text="RESET GAME", command=self.__resetGame)
        reset_btn.pack()

        ## loop the game ##########################
        self.__mainWindow.mainloop()

        # End of initializer

    """
    FizbizGUI private functions: -----------------------------------------------------------------------
    """

    def __checkGuessedCard(self, event):
        # Print to Console for testing
        print("__checkGuessedCard() is called:")

        self.isUnknownCardFound = True if event.widget == self.__unknownCardAnswer_lbl else False

        # Show feedback to user everytime he/she tries to match a card
        self.__showFeedback(self.isUnknownCardFound)

        # If Yoda Found the card, prompt a congrats message
        if self.__Yoda.isTurn and self.isUnknownCardFound:
            self.__openTheUnknownCard(event)
            messagebox.showinfo("CONGRATULATIONS", "YODA WINS\nGAME OVER")

        # If Yoda could not find the card, then subtract his score and let R2D2 play
        elif self.__Yoda.isTurn and self.isUnknownCardFound==False:
            self.__Yoda.subtractScores(pt=1)
            self.YodasScore_lbl["text"] = self.__Yoda.getPlayersScores()
            self.__Yoda.isTurn = False
            self.__Robot.isTurn = True
            self.__changeBackgroundColor(self.Yoda_lbl, "white")
            self.__changeBackgroundColor(self.Robot_lbl, "PaleTurquoise1")

        # If RT2D2 Found the card, prompt a congrats message
        elif self.__Robot.isTurn and self.isUnknownCardFound:
            self.__openTheUnknownCard(event)
            messagebox.showinfo("CONGRATULATIONS", "R2D2 WINS.\nGAME OVER")

        # If R2D2 could not find the card, then subtract his score and let YODA play
        elif self.__Robot.isTurn and self.isUnknownCardFound==False:
            self.__Robot.subtractScores(pt=1)
            self.RobotsScore_lbl["text"] = self.__Robot.getPlayersScores()
            self.__Robot.isTurn = False
            self.__Yoda.isTurn = True
            self.__changeBackgroundColor(self.Yoda_lbl, "yellow")
            self.__changeBackgroundColor(self.Robot_lbl, "white")

    def __openTheUnknownCard(self, event):
        # Print to Console for testing
        print("__openTheUnknownCard() is called:")
        print(self.__unknownCard_lbl["image"], self.__unknownCardAnswer_lbl["image"])

        self.__isUnknownCardOpen = True
        print("isUnknownCardOpen=", self.__isUnknownCardOpen)

        # Open the card
        self.__unknownCard_lbl["image"] = self.__unknownCardAnswer_lbl["image"]

    def __showFeedback(self, isUnknownCardFound):
        nopeMsgs = ["NOPE", "VERY CLOSE", "ALMOST", "TRY AGAIN"]
        yesMsgs = ["YAHOO!!!", "WOHOOO", "THERE YOU GO!"]

        if isUnknownCardFound == False:
            self.__gameGreeting["text"] = "-\n" + nopeMsgs[ random.randint( 1,len(nopeMsgs)-1 ) ] + "\n"
        else:
            self.__gameGreeting["text"] = "-\n" + yesMsgs[random.randint(1, len(yesMsgs) - 1)] + "\n"

    def __changeBackgroundColor(self, widget, color):
        widget["bg"] = color

    def __resetGame(self):
        self.__mainWindow.destroy()
        self.__init__()


FizbizGUI()
