"""
Fikri Ghazi
CIS177
June 24, 2016

Program: Just a class to manage Player's data
Description: A Tkinter game about finding a match for an unknown card.
"""

class Player:
    def __init__(self, name="Player_1", score=0, turn=False):
        self.__name = name
        self.__scores = score
        self.isTurn = turn


    def addScores(self, pt=1):
        self.__scores += pt

    def subtractScores(self, pt=1):
        self.__scores -= pt

    def getPlayersName(self):
        return self.__name

    def getPlayersScores(self):
        return self.__scores

# yoda = Player(name="Yoda")
# print(yoda.getPlayersName())
