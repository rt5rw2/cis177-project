# FIZBIZ Game Instructions

## What's in the game:
0. Cards
1. Yoda
2. R2D2
3. Scores

## Game Description:
This game is not about winning or having fun. This game is about friendship.

To play this game you must be prepared with mentally. And for those who don't prepare--congratulations you played yourselves.

There will be 10 open cards, and one mysterious card. And the goal is to find what lies beneath the mysterious one before anyone else find it.

## How to play:
0. Find a human and ask him/her to play
1. Do a rock-paper-scissor
2. The human who wins rock-paper-scissor must go first, and this human will become Yoda.
3. The human who loses rock-paper-scissor become R2D2
4. Yoda must click on one of the open cards
5. After Yoda, RT2D2 must follow the same thing
6. Keep taking turns until there's a miracle which helps to find the right card.
7. Congratulations you’ve played yourself

## Note:
This program works--but on the design and organization sides, it is far from what I have expected. For instance I want to have modular project such that codes aren't condensed in one file. But Because of my lack understanding in Tkinter this has been a problem. As a result I have a very messy project that works.